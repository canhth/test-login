import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

import 'auth_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              onPressed: () {
                _getProfile();
              },
              child: const Text('Get profile'),
            ),
            TextButton(
              onPressed: () {
                _logout(() {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => const AuthPage(),
                  ));
                });
              },
              child: const Text('Log out'),
            ),
          ],
        ),
      ),
    );
  }

  FutureOr<void> _logout(VoidCallback onCallback) async {
    try {
      const channel = MethodChannel("com.example.test_login/zalo");

      await channel.invokeMethod('logout');

      onCallback.call();
    } catch (e) {
      log(e.toString());
    }
  }

  FutureOr<void> _getProfile() async {
    const channel = MethodChannel("com.example.test_login/zalo");

    final data = await channel.invokeMethod('getProfile');
    final logger = Logger();

    logger.i(data);
  }
}
